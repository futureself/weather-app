exports.definition = {
  config: {
    columns: {
        "location": "String",
        "celsius": "Number",
        "kelvin": "Number",
        "fahrenheit": "Number"
    },
    defaults: {
      "location": "",
      "celsius": 0,
      "kelvin": 0,
      "fahrenheit": 0
    },
    adapter: {
      type: "properties",
      collection_name: "weatherReport"
    }
  },
  extendModel: function(Model) {
    _.extend(Model.prototype, {
      // extended functions and properties go here
    });

    return Model;
  },
  extendCollection: function(Collection) {
    _.extend(Collection.prototype, {
      // extended functions and properties go here
    });

    return Collection;
  }
};
