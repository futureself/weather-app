

var COOL_COLOR_HEX = "";  // FIXME: Define
var HOT_COLOR_HEX = "";   // FIXME: Define
var COLD_THRESHOLD = 6;  // degrees celsius
var HOT_THRESHOLD = 30;  // degrees celsius

function WeatherGradient(coldColorHex, hotColorHex, coldThreshold, hotThreshold) {
  var cool = removeLeadingSharp(coldColorHex),
      hot  = removeLeadingSharp(hotColorHex);

  this.coldThreshold = (coldThreshold === undefined) ?
    COLD_THRESHOLD : coldThreshold;
  this.hotThreshold = (hotThreshold === undefined) ?
    HOT_THRESHOLD : hotThreshold;

    // TODO: implement
    // ...
}

// Ultra-minimal optimization over just replacing "#"
function removeLeadingSharp(val) {
  return val[0] === "#" ? val.substring(1) : val;
}


module.exports = WeatherGradient;
