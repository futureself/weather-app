/**
* API provided by Open Weather Map
* http://openweathermap.org/api
*
* Terms on 12/17/2014 are the following:
*
* Pricing: Free up to 3000 calls / min and 4M calls / day
*
* License: Attribution, ShareAlike
*   http://creativecommons.org/licenses/by-sa/2.0/
*
* Notes:
*   Temperatures are in Kelvin
*/
var AbstractWeather = require('service/weather/AbstractWeatherService');

var URL = "http://api.openweathermap.org/data/2.5/weather?";

function OpenWeather() {
  AbstractWeather.apply(this, arguments);
}

OpenWeather.prototype = Object.create(AbstractWeather.prototype, {
  constructor: OpenWeather
});

// Override to declare what fetch modes this provider supports.
// param mode is one key in FetchMode, defined in AbstractWeatherService.
OpenWeather.prototype.supportsFetchMode = function(mode) {
  // Open Weather supports fetch by city id, name, and coordinates
  // but only fetch by city name and coordinates are supported here.
  var supported = this.FetchMode.CityName | this.FetchMode.Coordinates;
  return mode & supported;
};

OpenWeather.prototype.buildURL = function() {
  var url = URL;
  var error  = false;
  if (this.FetchMode.CityName === this.fetchMode) {
    // We are fetching by city name
    if (this.location !== null) {
      url += "q=" + encodeURI(this.location);
    } else {
      Ti.API.error("The weather provider cannot build the url without a " +
        "city name when fetching by city name");
      error = true;
    }
  } else if (this.FetchMode.Coordinates === this.fetchMode) {
    // We are fetching by coordinates
    if (this.latitude !== null && this.longitude !== null) {
      url += "lat=" + this.latitude + "&lon=" + this.longitude;
    } else {
      Ti.API.error("The weather provider cannot build the url without" +
        "a latitude and longitude when fetching by coordinates");
      error = true;
    }
  }
  return error ? null : url;
};

OpenWeather.prototype.setLocation = function(location) {
  // TODO: Sanitize
  this.location = location;
};

OpenWeather.prototype.parseResponse = function(responseObject) {
  Ti.API.info("Parsing response...");
  var response = responseObject;
  if (response.cod === "404") {
    Ti.API.debug("Error loading " + this.location);
  } else {
    Ti.API.info("Success temperature is " + response.main.temp);
    this.kelvin = response.main.temp;
    this.notify('service.weather.change', {
      location: this.location,
      kelvin: this.kelvin,
      celsius: this.kelvinToCelsius(this.kelvin),
      fahrenheit: this.kelvinToFahrenheit(this.kelvin)
    });
  }
};

module.exports = OpenWeather;
