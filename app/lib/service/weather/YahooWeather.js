var URL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20" +
  "weather.forecast%20where%20woeid%20in%20(select%20woeid%20from" +
  "%20geo.places(1)%20where%20text%3D%22{REPLACE_TOKEN}%22)&format=json" +
  "&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

var REPLACE_TOKEN = "{REPLACE_TOKEN}";

var AbstractWeather = require('service/weather/AbstractWeatherService');

function YahooWeather() {
  AbstractWeather.apply(this, arguments);
}
YahooWeather.prototype = Object.create(AbstractWeather.prototype, {
  constructor: YahooWeather
});

// Override to declare what fetch modes this provider supports.
// param mode is one key in FetchMode, defined in AbstractWeatherService.
YahooWeather.prototype.supportsFetchMode = function(mode) {
  var supported = this.FetchMode.CityName | this.FetchMode.Coordinates;
  return mode & supported;
};

YahooWeather.prototype.buildURL = function() {
  var url = URL;
  var error = false;
  if (this.FetchMode.CityName === this.fetchMode) {
    if (this.location !== null) {
      url = URL.replace(REPLACE_TOKEN, encodeURI(this.location));
    } else {
      Ti.API.error("The Yahoo provider needs a location to build the request");
      error = true;
    }
  } else if (this.FetchMode.Coordinates === this.fetchMode) {
    if (this.latitude !== null && this.longitude !== null) {
      url = URL.replace(REPLACE_TOKEN, this.latitude + "-" + this.longitude);
    } else {
      Ti.API.error("The weather provider cannot build the url without" +
      "a latitude and longitude when fetching by coordinates");
      error = true;
    }
  }
  return error ? null : url;
};

YahooWeather.prototype.parseResponse = function(responseObject) {
  Ti.API.info("Parsing response...");
  var response = responseObject;
  if (response.query.results === null) {
    Ti.API.debug("Error loading " + this.location);
  } else {
    var report = response.query.results.channel.item.condition;
    var temperature = report.temp;
    var description = report.text;
    Ti.API.info("Success temperature is " + temperature);
    this.fahrenheit = temperature;
    this.notify('service.weather.change', {
      location: this.location,
      celsius: Math.round(this.fahrenheitToCelsius(this.fahrenheit)),
      fahrenheit: Math.round(this.fahrenheit)
    });
  }
};

module.exports = YahooWeather;
