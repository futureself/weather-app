
var AbstractService = require('service/AbstractService');

function AbstractWeatherService() {
  AbstractService.apply(this, arguments);

  Object.defineProperty(this, "location", {
    get: function() { return _location; },
    set: function(location) {
      // TODO: Sanitize
      _location = location;
    }
  });

  Object.defineProperty(this, "latitude", {
    get: function() { return _latitude; },
    set: function(val) { _latitude = val; }
  });

  Object.defineProperty(this, "longitude", {
    get: function() { return _longitude; },
    set: function(val) { _longitude = val; }
  });

  Object.defineProperty(this, "fetchMode", {
    get: function() { return _fetchMode; },
    set: function(mode) {
      // TODO: Sanitize
      _fetchMode = mode;
    }
  });

  var _location = null;
  var _latitude = null;
  var _longitude = null;
  var _fetchMode = null;
}

AbstractWeatherService.prototype = Object.create(AbstractService.prototype);

AbstractWeatherService.prototype.Units = {
  Kelvin: "K",
  Fahrenheit: "F",
  Celsius: "C"
};

AbstractWeatherService.prototype.FetchMode = {
  CityName:     1 << 0,
  CityId:       1 << 1,
  Coordinates:  1 << 2
};

AbstractWeatherService.prototype.supportsFetchMode = function(mode) {
  // Override in concrete subtypes
  return false;
};

AbstractWeatherService.prototype.kelvinToFahrenheit = function(K) {
  return (this.kelvinToCelsius(K) * 2) + 30;
};

AbstractWeatherService.prototype.kelvinToCelsius = function(K){
  return K - 273.15;
};

AbstractWeatherService.prototype.fahrenheitToCelsius = function(F) {
  return (F - 32) * (5/9);
};

AbstractWeatherService.prototype.onLoad = function(e) {
  Ti.API.info("Weather service onload called");
  var responseText = e.source.responseText;
  if (responseText === null) {
    notify('service.weather.error', {
      message: "Unable to obtain response"
    });
  } else {
    var response = JSON.parse(responseText);
    this.parseResponse(response);
  }
  this.notify('service.weather.load');
};

AbstractWeatherService.prototype.onError = function(e) {
  Ti.API.info("Weather service onerror called");
  this.notify('service.weather.error', e);
};

AbstractWeatherService.prototype.setLocation = function(location) {
  // TODO: Sanitize
  this.location = location;
};

module.exports = AbstractWeatherService;
