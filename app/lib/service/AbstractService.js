/**
 *
 */

/**
 * Defaults
 */
var TIMEOUT = 8000;

/**
 *
 */
function AbstractService(timeout, notificationService) {
  if (this.constructor === AbstractService) {
    throw "This is an abstract type that can't be instantiated," +
    "use one of its concrete subtypes instead";
  }
  this.timeout = timeout || TIMEOUT;  // milliseconds
  this.notificationService = notificationService || null;
}

// Requires buildURL to return a valid value, otherwise it quits without fetching.
// TODO: Throw appropriate exceptions.
AbstractService.prototype.fetch = function() {
  Ti.API.info("Fetching data...");

  var self = this;
  var url = this.buildURL();

  if (url === null) return;

  var client = Ti.Network.createHTTPClient({
    onload: function(e) {
      Ti.API.info("Finished loading " + url);
      self.onLoad(e);
    },
    onerror: function(e) {
      Ti.API.debug("Error loading " + url);
      self.onError(e);
    },
    timeout: this.timeout
  });
  client.open("GET", url);
  client.send();
  this.onSend(url);
};

/* -------------- Abstract methods -------------- */

// required
AbstractService.prototype.buildURL = function() {
  throw "Concrete subtypes must implement buildUrl";
};

// optional
AbstractService.prototype.onLoad = function() {
  throw "Concrete subtypes must implement onLoad";
};

// optional
AbstractService.prototype.onError = function(e) {
  Ti.API.error("Error loading data: " + e);
};

// optional
AbstractService.prototype.onSend = function(url) {
  Ti.API.info("Request sent: " + url);
};

/* -------------- END of abstract methods ------- */

AbstractService.prototype.notify = function(eventName, obj) {
  Ti.API.info("Notifying event " + eventName);
  Ti.API.debug(obj);
  if (this.notificationService !== null) {
    this.notificationService.trigger(eventName, obj);
  }
};


module.exports = AbstractService;
