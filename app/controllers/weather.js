var args = arguments[0] || {};

var WeatherProvider = require('service/weather/OpenWeather');
var NotificationCenter = require('NotificationCenter');

// The number of times we try to get data from the weather
// provider before quitting.
var MAX_FETCH_FAILS = 3;
// The time we wait to get weather data after an error.
var FETCH_INTERVAL_ON_ERROR = 5000;  // milliseconds

var _weatherProvider = new WeatherProvider();
_weatherProvider.notificationService = NotificationCenter;

var _weatherReport = Alloy.createModel("weatherReport");

var fetchAttempts = 0;

NotificationCenter.bind('service.weather.change', function(report) {
  Ti.API.info("Weather changed");
  fetchAttempts = 0;  // reset counter on fetch success
  setWeatherReport(report);
});

NotificationCenter.bind('service.weather.error', function(e) {
  ++fetchAttempts;
  if (fetchAttempts >= MAX_FETCH_FAILS) {
    alert('Unable to fetch the weather at this time');
  }
  if (Ti.Network.NETWORK_NONE === Ti.Network.networkType) {
    // There is a network error, so try when it's available
    fetchWeatherReportWhenNetworkIsAvailable();
  } else {
    // Something must have gone wrong with the weather provider
    if (fetchAttempts <= MAX_FETCH_FAILS) {
      setTimeout(function() {
        _weatherProvider.fetch();
      }, FETCH_INTERVAL_ON_ERROR);
    }
  }
});

function fetchWeatherReport() {
  if (Ti.Network.networkType !== Ti.Network.NETWORK_NONE) {
    Ti.API.info("Fetching weather report...");
    _weatherProvider.fetch();
  } else {
    Ti.API.info("network is unavailable, trying later");
    fetchWeatherReportWhenNetworkIsAvailable();
  }
}

var listeningToNetworkChanges = false;
function fetchWeatherReportWhenNetworkIsAvailable() {
  if (!listeningToNetworkChanges) {
    listeningToNetworkChanges = true;
    Ti.Network.addEventListener('change', onNetworkChange);
  }
}
function onNetworkChange(e) {
  Ti.API.info('onNetworkChange');
  if (Ti.Network.networkType !== Ti.Network.NETWORK_NONE) {
    Ti.API.info('Network possibly available');
    Ti.Network.removeEventListener('change', onNetworkChange);
    listeningToNetworkChanges = false;
    setTimeout(fetchWeatherReport, 3000);
  }
}

function setWeatherReport(report) {
  // TODO: Sanitize report

  Ti.API.info("Got report:");
  Ti.API.debug(report);

  var modelReport = Alloy.createModel('weatherReport', report);
  stopListeningToModel();  // Remove bindings to old model before reassigning
  _weatherReport = modelReport;

  updateUI();
}

function setCoordinates(latitude, longitude) {
  if (_weatherProvider.supportsFetchMode(_weatherProvider.FetchMode.Coordinates)) {
    Ti.API.info("Setting coordinates:" + latitude + ", " + longitude);
    _weatherProvider.fetchMode = _weatherProvider.FetchMode.Coordinates;
    _weatherProvider.latitude = latitude;
    _weatherProvider.longitude = longitude;
    _weatherProvider.location = "Current Location";
    fetchWeatherReport();
  } else {
    Ti.API.error("Can't set coordinates because the current weather provider " +
      "does not support the 'Coordinates' mode");
  }
}

function setLocation(location) {
  // TODO: Sanitize input
  if (_weatherProvider.supportsFetchMode(_weatherProvider.FetchMode.CityName)) {
    Ti.API.info("Setting location: " + location);
    _weatherProvider.fetchMode = _weatherProvider.FetchMode.CityName;
    _weatherProvider.location = location;
    fetchWeatherReport();
  } else {
    Ti.API.error("Can't set location because the current weather provider " +
      "does not support the 'CityName' mode");
  }
}

function stopListeningToModel() {
  // Remove any callbacks we added to the model
}

function updateTemperature() {
  if(_weatherReport) {Ti.API.info("Temperature is " + _weatherReport.get("celsius")); }
  $.temperature.text = (_weatherReport === null) ?
    "" : Math.round(_weatherReport.get("celsius")) + "º";
}

function updateLocation() {
  if(_weatherReport) { Ti.API.info("Location is " + _weatherReport.get("location")); }
  // Whenever we don't have a location we are searching for the report
  // matching the current latitude and longitude
  $.location.text = (_weatherReport === null) ?
    "" : _weatherReport.get("location") || "Current Location";
}

function updateActivityIndicator() {
  _weatherReport === null ? $.loader.show() : $.loader.hide();
}

function updateUI() {
  Ti.API.info("Updating UI");
  updateActivityIndicator();
  updateTemperature();
  updateLocation();
}

// FIXME: Do memory cleanup if binding to model
// ...


function init() {
  Ti.API.info("Weather report on init:");
  Ti.API.debug(_weatherReport);
  if (_weatherReport === null) {
    fetchWeatherReport();
  }
  updateUI();
}


init();

exports.setWeatherReport = setWeatherReport;
exports.setLocation = setLocation;
exports.setCoordinates = setCoordinates;
