var args = arguments[0] || {};

var Providers = {
  OpenWeather: {
    title: "Open Weather",
    module: "service/weather/OpenWeather"
  },
  Yahoo: {
    title: "Yahoo",
    module: "service/weather/YahooWeather"
  }
};

var DEFAULT_PROVIDER_TITLE = Providers.OpenWeather.title;

var NotificationCenter = require('NotificationCenter');

$.providerList.addEventListener('itemclick', onItemClick);
function onItemClick(e) {
  Ti.API.info("weather provider clicked...");
  var notifyProviderChange = false;
  var oldSelectedItem = selectedItem;

  var items = e.section.getItems();
  for (var i=0; i < items.length; ++i) {
    var newItem = e.section.getItemAt(i);
    newItem.properties.accessoryType = (e.itemIndex === i) ?
      Ti.UI.LIST_ACCESSORY_TYPE_CHECKMARK:
      Ti.UI.LIST_ACCESSORY_TYPE_NONE;
    e.section.updateItemAt(i, newItem);
    if (e.itemIndex === i) {
      selectedItem = newItem;
      if (selectedItem.properties.title !== oldSelectedItem.properties.title) {
        notifyProviderChange = true;
      }
    }
  }
  if (notifyProviderChange) {
    if (selectedItem.properties.title !== DEFAULT_PROVIDER_TITLE) {
      alert('Sorry, always loyal to ' + DEFAULT_PROVIDER_TITLE +
        " (for now, will implement changing provider soon)");
    }
    // NotificationCenter.trigger('weatherprovider.change', {
    //   module: providerForTitle(selectedItem.properties.title).module
    // });
  }
}

function providerForTitle(title) {
  for (var provider in Providers) {
    if (Providers[provider].title === title) {
      return Providers[provider];
    }
  }
  return null;
}

var items = [];
for (var provider in Providers) {

  Ti.API.info("provider title: " + Providers[provider].title);

  var title = (! selectedItem) ?
    DEFAULT_PROVIDER_TITLE :
    selectedItem.properties.title;

  var titleMatch = title === Providers[provider].title;
  var accessoryType = (titleMatch) ?
    Ti.UI.LIST_ACCESSORY_TYPE_CHECKMARK:
    Ti.UI.LIST_ACCESSORY_TYPE_NONE;

  items.push({
    properties: {
      title: Providers[provider].title,
      itemId: Providers[provider].module,
      accessoryType: accessoryType
    }
  });

}

Ti.API.info("setting items");
Ti.API.debug(items);
$.weatherProviders.setItems(items);

// Default to the first provider in the list
var selectedItem = $.providerList.getSections()[0].getItemAt(0);
