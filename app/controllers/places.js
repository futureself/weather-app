var args = arguments[0] || {};

var NotificationCenter = require('NotificationCenter');

var selectedPlaces = {};

$.placeList.addEventListener('itemclick', onItemClick);

// This is the callback to support selection of a single city
// The callback for multiple cities selection is commented out
// until that feature is implemented.
function onItemClick(e) {
  var item = e.section.getItemAt(e.itemIndex);
  var items = e.section.getItems();
  for (var i=0; i<items.length; ++i) {
    if (i !== e.itemIndex) {
      delete selectedPlaces[items[i].properties.title];
      items[i].properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_NONE;
    }
    e.section.updateItemAt(i, items[i]);
  }
  selectedPlaces[item.properties.title] = true;
  item.properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_CHECKMARK;
  e.section.updateItemAt(e.itemIndex, item);

  NotificationCenter.trigger('places.change', selectedPlaces);
}

/*
// This is the callback to support multiple cities selection
function onItemClick(e) {
  // Toggle item selection
  var item = e.section.getItemAt(e.itemIndex);

  Ti.API.info("Table cell:");
  Ti.API.debug(item);

  if (Ti.UI.LIST_ACCESSORY_TYPE_CHECKMARK === item.properties.accessoryType) {
    Ti.API.info("Removing checkmark");
    delete selectedPlaces[item.properties.title];
    item.properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_NONE;
  } else {
    Ti.API.info("Adding checkmark");
    selectedPlaces[item.properties.title] = true;
    item.properties.accessoryType = Ti.UI.LIST_ACCESSORY_TYPE_CHECKMARK;
  }
  e.section.updateItemAt(e.itemIndex, item);

  NotificationCenter.trigger('places.change', selectedPlaces);
}
*/


exports.selectedPlaces = selectedPlaces;
