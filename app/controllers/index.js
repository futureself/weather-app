$.index.open();

var NotificationCenter = require('NotificationCenter');

var selectedPlaces = {};

NotificationCenter.bind('places.change', function(selectedItems) {
  Ti.API.info("Index was notified of place changes");
  Ti.API.debug(selectedItems);

  // Do nothing, for now we'll postpone doing any work until the places
  // window loses focus.

});

NotificationCenter.bind('weatherprovider.update', function() {
  Ti.API.info("weather updated its provider");
  updatePlaces();
});

$.placesWindow.addEventListener('blur', onPlacesBlur);

function onPlacesBlur() {
  Ti.API.info("The places window lost focus");
  if (placesChanged()) {
    updatePlaces();
  } else {
    Ti.API.info("Places match, no need to update");
  }
}

function placesChanged() {
  return !_.isEqual(selectedPlaces, $.places.selectedPlaces);
}

function updatePlaces() {
  Ti.API.info("Updating places");
  selectedPlaces = _.extend({}, $.places.selectedPlaces);
  Ti.API.info("selectedPlaces:");
  Ti.API.debug(selectedPlaces);

  for (var place in selectedPlaces) {
    Ti.API.info(place);
    // TODO: Do not use a string
    if (place === "Current Location") {
      getCurrentPosition();
    } else {
      $.weather.setLocation(place);
    }
    return;
  }
}

function stopListeningForLocations() {
  Ti.API.info("Index will stop listening to location updates");
  Ti.Geolocation.removeEventListener('location', onLocationChange);
}

function onLocationChange(e) {
  Ti.API.info("Location changed:");
  Ti.API.debug(e);
  if (e !== null) {
    $.weather.setCoordinates(e.coords.latitude, e.coords.longitude);
  }
}

function configureLocationServices() {

  // I have a feeling that these two internally map to the same, but just in case...
  var accuracy = OS_IOS ?
      Ti.Geolocation.ACCURACY_THREE_KILOMETERS :
      Ti.Geolocation.ACCURACY_LOW;

  Ti.Geolocation.setAccuracy(accuracy);

  if (! Ti.Geolocation.locationServicesEnabled) {
    Ti.API.info("Location services are NOT enabled");
    // TODO: Deal with this
  }

  Ti.Geolocation.setTrackSignificantLocationChange(true);
}

function getCurrentPosition() {
  configureLocationServices();
  Ti.Geolocation.addEventListener('location', onLocationChange);
  Ti.Geolocation.getCurrentPosition(onLocationChange);
}

function init() {
  getCurrentPosition();
  //$.weather.setLocation("Amsterdam");
}



init();
